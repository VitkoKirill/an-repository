package com.example.bookshop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bookshop.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var _binding: ActivityMainBinding?=null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        _binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        _binding=null
//    }
//
//    companion object{
//        private const val FIRST_ITEM_TYPE=9999
//        private const val SECOND_ITEM_TYPE=9998
//    }
}